CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Requirements
 * Configuration
 * Creators
 * Maintainers


 INTRODUCTION
 ------------

The Unlimited/Multi-Value Fields Extra Settings module allows you to configure 
the number of initially visible items for multi-value fields.

 INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-8
    for further information.

 REQUIREMENTS
 ------------

  This module need no requirements.

 CONFIGURATION
 ------------
  After installing the module:
   1. Navigate to the form display of the desired entity.
      For example, for the "Article" content type: 
      <br/> **_Structure >> Content Types >> Manage form display_**.
   2. Locate the field you want to configure. 
      Ensure the field has a cardinality of "Unlimited" or greater than 1.
   3. Enter the desired number of items 
      in the _**Number of values to display**_ setting and save.

 CREATORS
 --------

 * BERRAMOU - https://www.drupal.org/user/3535998


  MAINTAINERS
  --------

  * BERRAMOU - https://www.drupal.org/user/3535998
  * Youness KARJOUH - https://www.drupal.org/user/3563813
  * Martin Anderson-Clutz - https://www.drupal.org/u/mandclu
