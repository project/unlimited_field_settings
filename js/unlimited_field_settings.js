(function ($) {
  'use strict';

  Drupal.behaviors.unlimited_field_settings = {
    attach: function (context, settings) {
      let cardinality = settings?.cardinality ?? 0;
      $('input[name*="unlimited_field_settings"]').on('change', function () {
        if (cardinality > 1 && cardinality < $(this).val()) {
          alert('Please set the minimum number of items to display less than the field cardinality :' + cardinality);
        }
      });
    },
  };
}(jQuery));
